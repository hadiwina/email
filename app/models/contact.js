import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { hasMany } from 'ember-data/relationships';
import Ember from 'ember';

const { computed } = Ember;

export default Model.extend({
    name: attr('string'),
    phoneNumber: hasMany('phone-number'),
    emailAddress: hasMany('email-address'),

    hasMoreEmailAddresses: computed('emailAddress', function () {
        return this.get('emailAddress.length') > 1;
    }),

    additionalEmailAddresses: computed('hasMoreEmailAddresses', function () {
        const hasMoreEmails = this.get('hasMoreEmailAddresses');
        return hasMoreEmails ? this.get('emailAddress.length')-1 : 0;
    }),

    hasMorePhoneNumbers: computed('phoneNumber', function () {
        return this.get('phoneNumber.length') > 1;
    }),

    additionalPhoneNumbers: computed('hasMorePhoneNumbers', function () {
        const hasMoreEmails = this.get('hasMorePhoneNumbers');
        return hasMoreEmails ? this.get('phoneNumber.length')-1 : 0;
    })
});
