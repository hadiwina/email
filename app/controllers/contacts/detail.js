import Ember from 'ember';

const { computed  } = Ember;

export default Ember.Controller.extend({
    contact: computed.alias('model'),

    viewMode: true,

    actions: {
        editContact() {
            this.set('viewMode', false);
        },

        submitUpdate() {
            this.get('model').save().then(() => {
                let store = this.get('store');

                store.peekAll('email-address').forEach((emailAddress) => {
                    if (emailAddress.get('hasDirtyAttributes')) {
                        emailAddress.save();
                    }
                });

                store.peekAll('phone-number').forEach((phoneNumber) => {
                    if (phoneNumber.get('hasDirtyAttributes')) {
                        phoneNumber.save();
                    }
                })

                this.set('viewMode', true);
            }).catch((exceptions) => {
                this.set('exceptions', exceptions)
            });
        },

        cancelEdit() {
            let model = this.get('model');
            model.rollbackAttributes();
            model.get('emailAddress').forEach(email => email.rollbackAttributes());
            model.get('phoneNumber').forEach(number => number.rollbackAttributes());
            this.set('viewMode', true);
        }
    }
});
