import { Factory, faker, hasMany } from 'ember-cli-mirage';

export default Factory.extend({
    name() { return faker.name.findName(); }
});
