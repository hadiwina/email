import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    contactId: null,
    type: null,
    email() { return faker.internet.email(); }
});
