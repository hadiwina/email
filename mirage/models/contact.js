import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
    phoneNumber: hasMany('phone-number'),
    emailAddress: hasMany('email-address')
});
