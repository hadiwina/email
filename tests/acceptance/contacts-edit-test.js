import { test, skip } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';
import Mirage from 'ember-cli-mirage';

moduleForAcceptance('Acceptance | contacts edit');

test('Edit contact displays contact current info', function(assert) {
  // set up test data
  let contact = server.create('contact', { name: 'Ann Smith' });
  server.create('email-address', { contactId: contact.id, email: 'smith@example.com' });
  server.create('email-address', { contactId: contact.id, email: 'smith@example.org' });
  server.create('email-address', { contactId: contact.id, email: 'asmith@example.edu' });
  server.create('phone-number', { contactId: contact.id, number: '+15555555252' });
  server.create('phone-number', { contactId: contact.id, number: '+15555550002' });

  visitEditContact(contact);

  andThen(function() {
    assert.contains('.test-contact-name', 'Ann Smith', 'Contact name is displayed');
    assert.contains('.test-contact-phone', '+15555555252', 'Contact primary phone is displayed');
    assert.contains('.test-contact-phone', '+15555550002', 'Contact alternate phone is displayed');
    assert.contains('.test-contact-email', 'smith@example.com', 'Contact primary email is displayed');
    assert.contains('.test-contact-email', 'smith@example.org', 'Contact alternate email is displayed');
    assert.contains('.test-contact-email', 'asmith@example.edu', 'Contact work email is displayed');
  });
});

test('Edit contact allows editing contact info', function(assert) {
  // set up test data
  let contact = server.create('contact', { name: 'Ann Smith' });
  server.create('email-address', { contactId: contact.id, email: 'smith@example.com' });
  server.create('phone-number', { contactId: contact.id, number: '+15555555252' });

  visitEditContact(contact);

  // test actions
  click('.btn-edit-contact');

  fillIn('.edit-name', 'James Bond');
  fillIn('.edit-email', 'some@email.com');
  fillIn('.edit-phone', '+1 555-6666');

  click('.btn-edit-submit');

  andThen(function() {
    // fill in the assertions

    assert.equal(server.db.contacts[0].name, 'James Bond', 'Contact name is updated');

    assert.equal(server.db.phoneNumbers[0].number, '+1 555-6666', 'Primary phone is updated');

    assert.equal(server.db.emailAddresses[0].email, 'some@email.com', 'Primary email is updated');
  });
});

test('Edit contact handles server error when saving contact', function(assert) {
  // set up test data
  let contact = server.create('contact', { name: 'Ann Smith' });
  server.create('email-address', { contactId: contact.id, email: 'smith@example.com' })
  server.create('phone-number', { contactId: contact.id, number: '+15555555252' });

  server.patch('contacts/1', {errors: ['There was an error saving this contact. Please try again.']}, 500);

  visitEditContact(contact);

  // add test actions
  click('.btn-edit-contact');

  fillIn('.edit-name', 'James Bond');
  fillIn('.edit-email', 'some@email.com');
  fillIn('.edit-phone', '+1 555-6666');

  click('.btn-edit-submit');

  andThen(function() {
    // fill in assertions
    let contacts = server.db.contacts[0];
    let emailAddresses = server.db.emailAddresses[0];
    let phoneNumbers = server.db.phoneNumbers[0];

    assert.equal(contacts.name, 'Ann Smith', 'Contact name is NOT updated');

    assert.equal(phoneNumbers.number, '+15555555252', 'Primary phone is NOT updated');

    assert.equal(emailAddresses.email, 'smith@example.com', 'Primary email is NOT updated');

    assert.contains('.alert-danger', 'There was an error saving this contact. Please try again.');
  });
});

test('Cancelling Edit contact reserves old values', function(assert) {
  // set up test data
  let contact = server.create('contact', { name: 'Ann Smith' });
  server.create('email-address', { contactId: contact.id, email: 'smith@example.com' });
  server.create('phone-number', { contactId: contact.id, number: '+15555555252' });

  visitEditContact(contact);

  // add test actions
  click('.btn-edit-contact');

  fillIn('.edit-name', 'James Bond');
  fillIn('.edit-email', 'some@email.com');
  fillIn('.edit-phone', '+1 555-6666');

  click('.btn-edit-cancel');

  andThen(function() {
    // fill in assertions
    assert.contains('.test-contact-name', 'Ann Smith', 'Contact name is displayed');
    assert.contains('.test-contact-phone', '+15555555252', 'Contact primary phone is displayed');
    assert.contains('.test-contact-email', 'smith@example.com', 'Contact primary email is displayed');
  });
});

skip('Edit contact handles server error when saving contact phone, saving all related models other than the erroring one');

skip('Edit contact handles server error when saving contact email, saving all related models other than the erroring one');

skip('Edit contact does not allow empty name');

skip('Edit contact allows adding a phone');
skip('Edit contact allows adding an email');
skip('Edit contact allows adding multiple phones and/or emails at the same time');
skip('Edit contact allows removing emails');
skip('Edit contact allows removing phones');

// contact is a mirage factory
function visitEditContact(contact) {
  visit(`/contacts/${contact.id}`);
}
